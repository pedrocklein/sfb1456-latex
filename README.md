# SFB1456 Latex

This repository is built based on the docker image https://hub.docker.com/r/schruste/latex.
To run it, you must have docker installed on your computer, and you must download the image from the container registry.

## Downloading or creating the image
To download the image, you first need to login in the container registry from GWDG gitlab. For such, you must use the command `docker login docker.gitlab.gwdg.de`. You will be asked for your login and password, the login is your e-mail and the password should be an access token created by following the steps described [here](https://gitlab.gwdg.de/help/user/profile/personal_access_tokens.md) and creating an access token for the container registry.

Once you are logged in, then you can download the image using the command `docker pull docker.gitlab.gwdg.de/pedrocklein/sfb1456-latex:latest`.

Another option is cloning this repository locally and building the docker image using the command `docker build -t <IMAGE_NAME> .` (replacing <IMAGE_NAME> by a proper name for your image).

## Usage
To use it, navigate to the folder in which you want to work on (e.g. `/home/user/latex_project`) and run the docker image in a container, mapping your local folder to a folder inside the container. The following command can be used as an example:

`docker run -it --rm -v ./:/latex_project --name latex_test docker.gitlab.gwdg.de/pedrocklein/sfb1456-latex` (or `docker run -it --rm -v ./:/latex_project --name latex_test <IMAGE_NAME>` if you created the image locally)

In this example, your current folder will be mapped to the "latex_project" folder.